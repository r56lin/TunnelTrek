# Team-101-1
Note: The ZIP file contains the code of the final project, but not the work in the original repository used to create that project as it is a private repository.
## Name
TunnelTrek

## Description
TunnelTrek is an indoor navigation system, specializing in campus navigation, built on crowd-sourced map data.
It is intended to be able to support any building or close group of buildings.

## Screenshot
[Screenshot in Wiki](https://git.uwaterloo.ca/r56lin/TunnelTrek/-/wikis/Screenshot)

## Team names and contact info
Tanishq Bomman (tbomman@uwaterloo.ca)
Nathan Hamilton (n4hamilt@uwaterloo.ca)
Zong Gao Li (zgli@uwaterloo.ca)
Rayton Lin (r56lin@uwaterloo.ca)

## User Documentation (Screenshots of Feature functionality)
https://git.uwaterloo.ca/r56lin/TunnelTrek/-/wikis/User-Documentation

## Release Notes

Release Date: `05-Apr-2024`

Version: `4.0.0`

### Changes

- Authentication (login and create account)
- Upload maps
- Download maps
- Maps editor (allows users to edit existing maps)
- Major UI and stability improvements
- Error handling for UI, auth and cloud maps
- Enable use of maps without login
- Map editor persists


## Installation

Installers are in the Project's Root

`TunnelTrek.msi`
`TunnelTrek.apk`
## License
This is not an open source project, so there is no license.
